# The Quay CA root (The Quay CA Root Q2)
# we keep a separate configuraiton file so that we don't accidentally create
# a CA cert when using the intermediate signing configuration
dir = /home/gabriel/git/quayCA/root-q2
RANDFILE = $dir/../.random

[ ca ]
default_ca = root-q2

[ root-q2 ]
new_certs_dir                   = $dir/certs
unique_subject                  = no
certificate                     = $dir/certs/root-q2.crt
database                        = $dir/meta/index.txt
private_key                     = $dir/private/privkey.pem
serial                          = $dir/meta/serial.txt
default_days                    = 1096
default_md                      = sha256
policy                          = root-q2_policy
# certificate revocation lists
crlnumber                       = $dir/meta/crlnumber.txt
crl                             = $dir/crl/root-q2.crl
default_crl_days                = 90
crl_extensions                  = crl_ext
# by default this certificate is only used to sign other CA certs but you must
# specify the correct extension so we don't create a new CA signing cert
x509_extensions                 = root-q2_extensions

[ crl_ext ]
issuerAltName                   = URI:https://quay.net/pub/ca/root-q2.crt
authorityKeyIdentifier          = keyid:always,issuer
crlDistributionPoints           = URI:https://quay.net/pub/ca/root-q2.crl

[ root-q2_policy ]
commonName                      = supplied
stateOrProvinceName             = optional
countryName                     = optional
emailAddress                    = optional
organizationName                = optional
organizationalUnitName          = optional
localityName                    = optional

# general extensions for non CA certs (this should not normally be used)
[ root-q2_extensions ]
basicConstraints                = CA:false
subjectKeyIdentifier            = hash
authorityKeyIdentifier          = keyid:always,issuer
keyUsage                        = critical,digitalSignature,keyEncipherment
extendedKeyUsage                = serverAuth
# this should exist in all extensions
crlDistributionPoints           = URI:https://quay.net/pub/ca/root-q2.crl

# this extension is used to self-sign the root
[ v3_ca_root ]
subjectKeyIdentifier            = hash
authorityKeyIdentifier          = keyid:always,issuer:always
# certificates we sign can't create their own CA certs
basicConstraints                = critical,CA:true,pathlen:1
keyUsage                        = critical,digitalSignature,cRLSign,keyCertSign
crlDistributionPoints           = URI:https://quay.net/pub/ca/root-q2.crl

# this extension is used to sign intermediate signing certificates
[ v3_ca_sign ]
subjectKeyIdentifier            = hash
authorityKeyIdentifier          = keyid:always,issuer:always
# certificates we sign with a signing key can't sign other CA certs
basicConstraints                = critical,CA:true,pathlen:0
keyUsage                        = critical,digitalSignature,cRLSign,keyCertSign
crlDistributionPoints           = URI:https://quay.net/pub/ca/root-q2.crl

# request configuration sections
[ req ]
default_bits                    = 2048
default_md                      = sha256
default_keyfile                 = privkey.pem
distinguished_name              = req_distinguished_name
attributes                      = req_attributes

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = CA
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Ontario
localityName                    = Locality Name (eg, city)
localityName_default            = Toronto
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = The Quay
commonName                      = Common Name (eg, fully qualified host name)
commonName_max                  = 64

# this section is empty but must exist
[ req_attributes ]
